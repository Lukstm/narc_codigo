/*
 * encoder.c
 *
 *  Created on: 4 de nov de 2019
 *      Author: lukas
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "main.h"
#include "stm32l0xx_hal.h"
#include "encoder.h"


	volatile uint16_t cnt;

int testa(int a, int b)
{
	return a + b*b + a*__HAL_TIM_GET_COUNTER(&htim21);
}

void encoder_init ()
{
	 HAL_TIM_Encoder_Start(&htim21,TIM_CHANNEL_ALL);
	 __HAL_TIM_SET_COUNTER(&htim21,testa(123,1));
}
void encoder_loop ()
{
	cnt = __HAL_TIM_GET_COUNTER(&htim21);
}
