/*
 * usb_app.c
 *
 *  Created on: 4 de nov de 2019
 *      Author: lukas
 */

#include <stdint.h>
#include <stdbool.h>
#include "main.h"
#include "stm32l0xx_hal.h"
#include "usb_app.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "util_cbuf.h"


util_cbuf_data_t area[100];
util_cbuf_t cb;


void usb_app_rx_callback(uint8_t *buffer, uint32_t size)
{
	uint32_t n;

	for(n = 0 ; n < size ; n++)
		util_cbuf_put(&cb,buffer[n]);
}

void usb_app_loop(void)
{
	util_cbuf_data_t c;

	util_cbuf_init(&cb,area,100);

	while(1)
	{
		if(util_cbuf_get(&cb,&c) != UTIL_CBUF_EMPTY)
		{
			CDC_Transmit_FS(&c,1);
		}
	}
}
