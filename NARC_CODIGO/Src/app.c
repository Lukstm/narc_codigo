/*
 * app.c
 *
 *  Created on: 29 de abr de 2018
 *      Author: marcelo
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "nrf24.h"
#include "nrf24_app.h"
#include "hw.h"

volatile bool app_started = false;
volatile uint32_t last_recv_ms = 0;

nrf24_config_t nrf24_config =
{
	.channel = 17,
	.data_rate = nRF24_DR_2Mbps,
	.rdelay = nRF24_ARD_500us,
	.rcount = 3,
	.tx_power = nRF24_TXPWR_0dBm,
	.network_address = { 'T', 'E', 'S', '\0', '\0' },
	.address_size = 3,
};

bool app_is_started(void)
{
	return app_started;
}

void HAL_SYSTICK_Callback(void)
{
}

static void app_rx_cbk_func(nRF24_pipe_number_t pipe, uint8_t *buffer, uint8_t length, nrf24_tx_result_t status)
{
	if(status == NRF24_APP_OK)
	{
		last_recv_ms = hw_timer_get_tick_ms();

		if(buffer[0] == '1')
		{
			hw_user_led_toggle();
		}
	}
}

static void app_tx_cbk_func(nRF24_pipe_number_t pipe, uint8_t *buffer, uint8_t length, nrf24_tx_result_t status)
{
	if(status == NRF24_APP_OK)
	{
	}
}

void app_reset_radio(uint8_t rf_channel)
{
    nrf24_app_init(&nrf24_config,app_tx_cbk_func,app_rx_cbk_func);
    hw_nrf24_set_channel(rf_channel);
}

void app_key_pressed(void)
{
	static uint8_t buffer[32];
	buffer[0] = '1';

	nrf24_app_tx_data(buffer,32);
}

void app_init(void)
{
	hw_init();
    nrf24_app_init(&nrf24_config,app_tx_cbk_func,app_rx_cbk_func);

    app_started = true;
}

void app_loop(void)
{
	while(1)
	{
		hw_sleep();//suspende a execu��o at� que uma interrup��o ocorra
	}
}
