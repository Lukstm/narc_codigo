/*
 * motor.c
 *
 *  Created on: 21 de out de 2018
 *      Author: lukas
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "main.h"
#include "stm32l0xx_hal.h"
#include "motor.h"



static bool flipLeft = false;
static bool flipRight = false;

extern TIM_HandleTypeDef htim2;

//MOTOR A E B, DIRE��O 1 E 2

#define HW_MOTA_IN1_PORT   MOT1_IN1_GPIO_Port
#define HW_MOTA_IN1_PIN    MOT1_IN1_Pin
#define HW_MOTA_IN2_PORT   MOT1_IN2_GPIO_Port
#define HW_MOTA_IN2_PIN    MOT1_IN2_Pin
#define HW_MOTB_IN1_PORT   MOT2_IN1_GPIO_Port
#define HW_MOTB_IN1_PIN    MOT2_IN1_Pin
#define HW_MOTB_IN2_PORT   MOT2_IN2_GPIO_Port
#define HW_MOTB_IN2_PIN    MOT2_IN2_Pin

//PWM - TIMER E CHANNELS
#define HW_TIMER_PORT      htim2
#define TIM_CHANNEL_A      TIM_CHANNEL_1
#define TIM_CHANNEL_B 	   TIM_CHANNEL_2


void motor_init()
{
	HAL_TIM_Base_Start(&HW_TIMER_PORT);
	HAL_TIM_PWM_Start(&HW_TIMER_PORT, TIM_CHANNEL_A);//MOTA_PWM
	HAL_TIM_PWM_Start(&HW_TIMER_PORT, TIM_CHANNEL_B);//MOTB_PWM
}
void motor_parada()
{
	HAL_GPIO_WritePin(HW_MOTA_IN1_PORT, HW_MOTA_IN1_PIN, GPIO_PIN_RESET);//MOTOR ESQUERDA
	HAL_GPIO_WritePin(HW_MOTA_IN2_PORT, HW_MOTA_IN2_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(HW_MOTB_IN1_PORT, HW_MOTB_IN1_PIN, GPIO_PIN_RESET);//MOTOR DIREITA
	HAL_GPIO_WritePin(HW_MOTB_IN2_PORT, HW_MOTB_IN2_PIN,GPIO_PIN_RESET);
	__HAL_TIM_SET_COMPARE(&HW_TIMER_PORT, TIM_CHANNEL_A, 9999);//A
	__HAL_TIM_SET_COMPARE(&HW_TIMER_PORT, TIM_CHANNEL_B, 9999);//B
}

void setSpeeds(int leftSpeed, int rightSpeed)
{
  setLeftSpeed(leftSpeed);
  setRightSpeed(rightSpeed);
}

void setLeftSpeed(int speed)
{
  init(); // initialize if necessary
  bool reverse = 0;

    if (speed < 0)
    {
      speed = -speed;
      reverse = 1;
    }

  if (speed > 9999)  // Max
   speed = 9999;



  if (reverse ^ flipLeft)
  {
	   __HAL_TIM_SET_COMPARE(&HW_TIMER_PORT, TIM_CHANNEL_A, speed);
	  HAL_GPIO_WritePin(HW_MOTA_IN1_PORT, HW_MOTA_IN1_PIN, GPIO_PIN_SET);
      HAL_GPIO_WritePin(HW_MOTA_IN2_PORT, HW_MOTA_IN2_PIN, GPIO_PIN_RESET);


  }
  else
  {
	   __HAL_TIM_SET_COMPARE(&HW_TIMER_PORT, TIM_CHANNEL_A, speed);
	  HAL_GPIO_WritePin(HW_MOTA_IN1_PORT, HW_MOTA_IN1_PIN, GPIO_PIN_RESET);
      HAL_GPIO_WritePin(HW_MOTA_IN2_PORT, HW_MOTA_IN2_PIN, GPIO_PIN_SET);
  }


}


void setRightSpeed(int speed)
{
  init();

  bool reverse = 0;

    if (speed < 0)
    {
      speed = -speed;
      reverse = 1;
    }

 if (speed > 9999)
   speed = 9999;


  if (reverse ^ flipRight)
  {
      __HAL_TIM_SET_COMPARE(&HW_TIMER_PORT, TIM_CHANNEL_B, speed);
	  HAL_GPIO_WritePin(HW_MOTB_IN1_PORT, HW_MOTB_IN1_PIN, GPIO_PIN_SET);
      HAL_GPIO_WritePin(HW_MOTB_IN1_PORT, HW_MOTB_IN2_PIN, GPIO_PIN_RESET);

	//B
  }
  else
  {
      __HAL_TIM_SET_COMPARE(&HW_TIMER_PORT, TIM_CHANNEL_B, speed);
	  HAL_GPIO_WritePin(HW_MOTB_IN1_PORT, HW_MOTB_IN1_PIN, GPIO_PIN_RESET);
      HAL_GPIO_WritePin(HW_MOTB_IN1_PORT, HW_MOTB_IN2_PIN, GPIO_PIN_SET);
  }


}


void motor_loop (void)
{

	setSpeeds(9999, 9999);
}
