/*
 * usb_app.h
 *
 *  Created on: 4 de nov de 2019
 *      Author: lukas
 */

#ifndef USB_APP_H_
#define USB_APP_H_


void usb_app_loop(void);
void usb_app_rx_callback(uint8_t *buffer, uint32_t size);

#endif /* USB_APP_H_ */
