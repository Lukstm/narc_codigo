/*
 * motor.h
 *
 *  Created on: 21 de out de 2018
 *      Author: lukas
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include<stdbool.h>


    void motor_parada(void);
    void setLeftSpeed(int speed);
    void setRightSpeed(int speed);
    void setSpeeds(int leftSpeed, int rightSpeed);
    void motor_init();

    static inline void init()
    {
      static bool initialized = false;

      if (!initialized)
      {
        initialized = true;
        motor_init();
      }
    }

    void motor_loop (void);

#endif /* MOTOR_H_ */
