/*
 * encoder.h
 *
 *  Created on: 4 de nov de 2019
 *      Author: lukas
 */

#ifndef ENCODER_H_
#define ENCODER_H_

int testa(int a, int b);
void encoder_init (void);
void encoder_loop (void);

#endif /* ENCODER_H_ */
